﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;

namespace prjWinCsMaisonSensor
{
    public partial class frmPpal : Form
    {
        /// <summary>
        /// tableau representant le status des capteur
        /// du systemen domotique
        /// </summary>
        private bool[,] alarmVar;
        private static int ALARMTYPE = 4;
        private static int ALARMNUMERO = 8;
        private bool ConectionStatus = false;

        private SerialPort rs232;
        public frmPpal()
        {
            InitializeComponent();
            this.lblF1.Click += myLableHander;
            this.lblF2.Click += myLableHander;
            this.lblF3.Click += myLableHander;
            this.lblF4.Click += myLableHander;
            this.lblF5.Click += myLableHander;
            this.lblF6.Click += myLableHander;
            this.lblF7.Click += myLableHander;
            this.lblF8.Click += myLableHander;
            this.lblP1.Click += myLableHander;
            this.lblP2.Click += myLableHander;
            this.lblP3.Click += myLableHander;
            this.lblP4.Click += myLableHander;
            this.lblP5.Click += myLableHander;
            this.lblDm1.Click += myLableHander;
            this.lblDm2.Click += myLableHander;
            this.lblDi1.Click += myLableHander;
            this.lblDi2.Click += myLableHander;
            this.alarmVar = new  bool[4, 8];

            for (int i = 0; i < ALARMTYPE; i++)
                for (int j = 0; j < ALARMNUMERO; j++)
                        this.alarmVar[i, j] = false;
                            
            setAlarStateColor();
        }

        private void setOpenPort()
        {
            string[] listComPort = SerialPort.GetPortNames();
            Array.Sort(listComPort);
            
            this.rs232 = new SerialPort(listComPort[0], 9600, Parity.None, 8, StopBits.One);
            if (!this.rs232.IsOpen)
            {
                this.rs232.Open();
                MessageBox.Show("Le port com1 est maintenant ouvert");
            }
        }

        private void setClosePort()
        {
            if (this.rs232.IsOpen)
            {
                this.rs232.Close();
                MessageBox.Show("le port com1 est maintenant ferme");
            }
        }

        private void myLableHander(object sender, EventArgs e)
        {
            Label lbltemp = (Label)sender; //(int)val;
            // MessageBox.Show(lbltemp.Text);
            string id = lbltemp.Text;


            switch (id)
            {
                case "F1":
                    this.alarmVar[0, 0] = !this.alarmVar[0, 0] ? true : false;
                    sendData(0, 0, this.alarmVar[0, 0] ? 1:0);
                    break;
                case "F2":
                    this.alarmVar[0, 1] = !this.alarmVar[0, 1] ? true : false;
                    sendData(0, 1, this.alarmVar[0, 1] ? 1 : 0);
                    break;
                case "F3":
                    this.alarmVar[0, 2] = !this.alarmVar[0, 2] ? true : false;
                    sendData(0, 2, this.alarmVar[0, 2] ? 1 : 0);
                    break;
                case "F4":
                    this.alarmVar[0, 3] = !this.alarmVar[0, 3] ? true : false;
                    sendData(0, 3, this.alarmVar[0, 3] ? 1 : 0);
                    break;
                case "F5":
                    this.alarmVar[0, 4] = !this.alarmVar[0, 4] ? true : false;
                    sendData(0, 4, this.alarmVar[0, 4] ? 1 : 0);
                    break;
                case "F6":
                    this.alarmVar[0, 5] = !this.alarmVar[0, 5] ? true : false;
                    sendData(0, 5, this.alarmVar[0, 5] ? 1 : 0);
                    break;
                case "F7":
                    this.alarmVar[0, 6] = !this.alarmVar[0, 6] ? true : false;
                    sendData(0, 6, this.alarmVar[0, 6] ? 1 : 0);
                    break;
                case "F8":
                    this.alarmVar[0, 7] = !this.alarmVar[0, 7] ? true : false;
                    sendData(0, 7, this.alarmVar[0, 7] ? 1 : 0);
                    break;
                case "P1":
                    this.alarmVar[1, 0] = !this.alarmVar[1, 0] ? true : false;
                    sendData(1, 0, this.alarmVar[1, 0] ? 1 : 0);
                    break;
                case "P2":
                    this.alarmVar[1, 1] = !this.alarmVar[1, 1] ? true : false;
                    sendData(1, 1, this.alarmVar[1, 1] ? 1 : 0);
                    break;
                case "P3":
                    this.alarmVar[1, 2] = !this.alarmVar[1, 2] ? true : false;
                    sendData(1, 2, this.alarmVar[1, 2] ? 1 : 0);
                    break;
                case "P4":
                    this.alarmVar[1, 3] = !this.alarmVar[1, 3] ? true : false;
                    sendData(1, 3, this.alarmVar[1, 3] ? 1 : 0);
                    break;
                case "P5":
                    this.alarmVar[1, 4] = !this.alarmVar[1, 4] ? true : false;
                    sendData(1, 4, this.alarmVar[1, 4] ? 1 : 0);
                    break;
                case "DM1":
                    this.alarmVar[2, 0] = !this.alarmVar[2, 0] ? true : false;
                    sendData(2, 0, this.alarmVar[2, 0] ? 1 : 0);
                    break;
                case "DM2":
                    this.alarmVar[2, 1] = !this.alarmVar[2, 1] ? true : false;
                    sendData(2, 1, this.alarmVar[2, 1] ? 1 : 0);
                    break;
                case "DI1":
                    this.alarmVar[3, 0] = !this.alarmVar[3, 0] ? true : false;
                    sendData(3, 0, this.alarmVar[3, 0] ? 1 : 0);
                    break;
                case "DI2":
                    this.alarmVar[3, 1] = !this.alarmVar[3, 1] ? true : false;
                    sendData(3, 1, this.alarmVar[3, 1] ? 1 : 0);
                    break;
            }
            setAlarStateColor();
        
            //MessageBox.Show("Valores" + this.stateFenetres + " " + this.statePortes + " " + this.stateOutres);

        }

        private void sendData(int type, int number, int status)
        {
            Byte[] msgToSend = new Byte[3];
            if (ConectionStatus) { 
                try
                {
                    msgToSend[0] = Convert.ToByte(type);
                    msgToSend[1] = Convert.ToByte(number);
                    msgToSend[2] = Convert.ToByte(status);

                    this.rs232.Write(msgToSend, 0, 3);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        

        private void miOn_Click(object sender, EventArgs e)
        {
            this.ConectionStatus = !this.ConectionStatus;
            if (this.ConectionStatus)
            {
                miOn.Text = "Connection On";
                miOn.BackColor = Color.Green;
                setOpenPort();

                for (int i = 0; i < ALARMTYPE; i++)
                    for (int j = 0; j < ALARMNUMERO; j++)
                        sendData(i, j, this.alarmVar[i, j] ? 1 : 0);

            }
            else
            {
                miOn.Text = "Connection Off";
                miOn.BackColor =  Color.Red;
                setClosePort();
            }






        }

        private void miSettings_Click(object sender, EventArgs e)
        {
            Form frmConnection = new frmConnection();
            frmConnection.Show();
        }

        private void setAlarStateColor()
        {
           this.lblF1.BackColor = !this.alarmVar[0, 0] ? Color.Green : Color.Red;
            this.lblF2.BackColor = !this.alarmVar[0, 1] ? Color.Green : Color.Red;
            this.lblF3.BackColor = !this.alarmVar[0, 2] ? Color.Green : Color.Red;
            this.lblF4.BackColor = !this.alarmVar[0, 3] ? Color.Green : Color.Red;
            this.lblF5.BackColor = !this.alarmVar[0, 4] ? Color.Green : Color.Red;
            this.lblF6.BackColor = !this.alarmVar[0, 5] ? Color.Green : Color.Red;
            this.lblF7.BackColor = !this.alarmVar[0, 6] ? Color.Green : Color.Red;
            this.lblF8.BackColor = !this.alarmVar[0, 7] ? Color.Green : Color.Red;
            this.lblP1.BackColor = !this.alarmVar[1, 0] ? Color.Green : Color.Red;
            this.lblP2.BackColor = !this.alarmVar[1, 1] ? Color.Green : Color.Red;
            this.lblP3.BackColor = !this.alarmVar[1, 2] ? Color.Green : Color.Red;
            this.lblP4.BackColor = !this.alarmVar[1, 3] ? Color.Green : Color.Red;
            this.lblP5.BackColor = !this.alarmVar[1, 4] ? Color.Green : Color.Red;
            this.lblDm1.BackColor = !this.alarmVar[2, 0] ? Color.Green : Color.Red;
            this.lblDm2.BackColor = !this.alarmVar[2, 1] ? Color.Green : Color.Red;
            this.lblDi1.BackColor = !this.alarmVar[3, 0] ? Color.Green : Color.Red;
            this.lblDi2.BackColor = !this.alarmVar[3, 1] ? Color.Green : Color.Red;

        }
 


    }
}
