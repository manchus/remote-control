﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;

namespace prjWinCsMaisonSensor
{
    public partial class frmConnection : Form
    {
        private SerialPort rs232;
        public frmConnection()
        {
            InitializeComponent();
            
        }

        private void btnOpenPort_Click(object sender, EventArgs e)
        {
            string[] listComPort = SerialPort.GetPortNames();
            Array.Sort(listComPort);

            this.rs232 = new SerialPort(listComPort[0], 9600, Parity.None, 8, StopBits.One);
            if (!this.rs232.IsOpen)
            {
                this.rs232.Open();
                MessageBox.Show("Le port com1 est maintenant ouvert");
            }
        }

        private void btnClosePort_Click(object sender, EventArgs e)
        {
            if (this.rs232.IsOpen)
            {
                this.rs232.Close();
                MessageBox.Show("le port com1 est maintenant ferme");
            }
        }

        public void btnSendData_Click(object sender, EventArgs e)
        {
            Byte[] msgToSend = new Byte[3];
            try
            {
                msgToSend[0] = Convert.ToByte("255");
                msgToSend[1] = Convert.ToByte("123");
                msgToSend[2] = Convert.ToByte("123");

                this.rs232.Write(msgToSend, 0, 3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
