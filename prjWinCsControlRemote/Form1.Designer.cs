﻿
namespace prjWinCsControlRemote
{
    partial class frmControl
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblP1 = new System.Windows.Forms.Label();
            this.lblP5 = new System.Windows.Forms.Label();
            this.lblP4 = new System.Windows.Forms.Label();
            this.lblDm2 = new System.Windows.Forms.Label();
            this.lblP3 = new System.Windows.Forms.Label();
            this.lblF8 = new System.Windows.Forms.Label();
            this.lblDi2 = new System.Windows.Forms.Label();
            this.lblDm1 = new System.Windows.Forms.Label();
            this.lblDi1 = new System.Windows.Forms.Label();
            this.lblP2 = new System.Windows.Forms.Label();
            this.lblF2 = new System.Windows.Forms.Label();
            this.lblF3 = new System.Windows.Forms.Label();
            this.lblF4 = new System.Windows.Forms.Label();
            this.lblF5 = new System.Windows.Forms.Label();
            this.lblF6 = new System.Windows.Forms.Label();
            this.lblF7 = new System.Windows.Forms.Label();
            this.lblF1 = new System.Windows.Forms.Label();
            this.lbxOutput = new System.Windows.Forms.ListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.settingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miOn = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackgroundImage = global::prjWinCsControlRemote.Properties.Resources.PLANE2;
            this.groupBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.groupBox1.Controls.Add(this.lblP1);
            this.groupBox1.Controls.Add(this.lblP5);
            this.groupBox1.Controls.Add(this.lblP4);
            this.groupBox1.Controls.Add(this.lblDm2);
            this.groupBox1.Controls.Add(this.lblP3);
            this.groupBox1.Controls.Add(this.lblF8);
            this.groupBox1.Controls.Add(this.lblDi2);
            this.groupBox1.Controls.Add(this.lblDm1);
            this.groupBox1.Controls.Add(this.lblDi1);
            this.groupBox1.Controls.Add(this.lblP2);
            this.groupBox1.Controls.Add(this.lblF2);
            this.groupBox1.Controls.Add(this.lblF3);
            this.groupBox1.Controls.Add(this.lblF4);
            this.groupBox1.Controls.Add(this.lblF5);
            this.groupBox1.Controls.Add(this.lblF6);
            this.groupBox1.Controls.Add(this.lblF7);
            this.groupBox1.Controls.Add(this.lblF1);
            this.groupBox1.Location = new System.Drawing.Point(24, 35);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(595, 384);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Maison";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // lblP1
            // 
            this.lblP1.AutoSize = true;
            this.lblP1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblP1.Location = new System.Drawing.Point(276, 366);
            this.lblP1.Name = "lblP1";
            this.lblP1.Size = new System.Drawing.Size(22, 15);
            this.lblP1.TabIndex = 17;
            this.lblP1.Text = "P1";
            // 
            // lblP5
            // 
            this.lblP5.AutoSize = true;
            this.lblP5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblP5.Location = new System.Drawing.Point(295, 130);
            this.lblP5.Name = "lblP5";
            this.lblP5.Size = new System.Drawing.Size(22, 15);
            this.lblP5.TabIndex = 16;
            this.lblP5.Text = "P5";
            // 
            // lblP4
            // 
            this.lblP4.AutoSize = true;
            this.lblP4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblP4.Location = new System.Drawing.Point(354, 130);
            this.lblP4.Name = "lblP4";
            this.lblP4.Size = new System.Drawing.Size(22, 15);
            this.lblP4.TabIndex = 15;
            this.lblP4.Text = "P4";
            // 
            // lblDm2
            // 
            this.lblDm2.AutoSize = true;
            this.lblDm2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDm2.Location = new System.Drawing.Point(377, 185);
            this.lblDm2.Name = "lblDm2";
            this.lblDm2.Size = new System.Drawing.Size(32, 15);
            this.lblDm2.TabIndex = 14;
            this.lblDm2.Text = "DM2";
            // 
            // lblP3
            // 
            this.lblP3.AutoSize = true;
            this.lblP3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblP3.Location = new System.Drawing.Point(390, 231);
            this.lblP3.Name = "lblP3";
            this.lblP3.Size = new System.Drawing.Size(22, 15);
            this.lblP3.TabIndex = 13;
            this.lblP3.Text = "P3";
            // 
            // lblF8
            // 
            this.lblF8.AutoSize = true;
            this.lblF8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblF8.Location = new System.Drawing.Point(84, 366);
            this.lblF8.Name = "lblF8";
            this.lblF8.Size = new System.Drawing.Size(21, 15);
            this.lblF8.TabIndex = 12;
            this.lblF8.Text = "F8";
            // 
            // lblDi2
            // 
            this.lblDi2.AutoSize = true;
            this.lblDi2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDi2.Location = new System.Drawing.Point(18, 356);
            this.lblDi2.Name = "lblDi2";
            this.lblDi2.Size = new System.Drawing.Size(26, 15);
            this.lblDi2.TabIndex = 10;
            this.lblDi2.Text = "DI2";
            // 
            // lblDm1
            // 
            this.lblDm1.AutoSize = true;
            this.lblDm1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDm1.Location = new System.Drawing.Point(18, 148);
            this.lblDm1.Name = "lblDm1";
            this.lblDm1.Size = new System.Drawing.Size(32, 15);
            this.lblDm1.TabIndex = 9;
            this.lblDm1.Text = "DM1";
            // 
            // lblDi1
            // 
            this.lblDi1.AutoSize = true;
            this.lblDi1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDi1.Location = new System.Drawing.Point(199, 121);
            this.lblDi1.Name = "lblDi1";
            this.lblDi1.Size = new System.Drawing.Size(26, 15);
            this.lblDi1.TabIndex = 8;
            this.lblDi1.Text = "DI1";
            // 
            // lblP2
            // 
            this.lblP2.AutoSize = true;
            this.lblP2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblP2.Location = new System.Drawing.Point(345, 263);
            this.lblP2.Name = "lblP2";
            this.lblP2.Size = new System.Drawing.Size(22, 15);
            this.lblP2.TabIndex = 7;
            this.lblP2.Text = "P2";
            // 
            // lblF2
            // 
            this.lblF2.AutoSize = true;
            this.lblF2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblF2.Location = new System.Drawing.Point(18, 104);
            this.lblF2.Name = "lblF2";
            this.lblF2.Size = new System.Drawing.Size(21, 15);
            this.lblF2.TabIndex = 6;
            this.lblF2.Text = "F2";
            // 
            // lblF3
            // 
            this.lblF3.AutoSize = true;
            this.lblF3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblF3.Location = new System.Drawing.Point(421, 37);
            this.lblF3.Name = "lblF3";
            this.lblF3.Size = new System.Drawing.Size(21, 15);
            this.lblF3.TabIndex = 5;
            this.lblF3.Text = "F3";
            // 
            // lblF4
            // 
            this.lblF4.AutoSize = true;
            this.lblF4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblF4.Location = new System.Drawing.Point(557, 203);
            this.lblF4.Name = "lblF4";
            this.lblF4.Size = new System.Drawing.Size(21, 15);
            this.lblF4.TabIndex = 4;
            this.lblF4.Text = "F4";
            // 
            // lblF5
            // 
            this.lblF5.AutoSize = true;
            this.lblF5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblF5.Location = new System.Drawing.Point(481, 366);
            this.lblF5.Name = "lblF5";
            this.lblF5.Size = new System.Drawing.Size(21, 15);
            this.lblF5.TabIndex = 3;
            this.lblF5.Text = "F5";
            // 
            // lblF6
            // 
            this.lblF6.AutoSize = true;
            this.lblF6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblF6.Location = new System.Drawing.Point(421, 366);
            this.lblF6.Name = "lblF6";
            this.lblF6.Size = new System.Drawing.Size(21, 15);
            this.lblF6.TabIndex = 2;
            this.lblF6.Text = "F6";
            // 
            // lblF7
            // 
            this.lblF7.AutoSize = true;
            this.lblF7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblF7.Location = new System.Drawing.Point(144, 366);
            this.lblF7.Name = "lblF7";
            this.lblF7.Size = new System.Drawing.Size(21, 15);
            this.lblF7.TabIndex = 1;
            this.lblF7.Text = "F7";
            // 
            // lblF1
            // 
            this.lblF1.AutoSize = true;
            this.lblF1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblF1.Location = new System.Drawing.Point(18, 203);
            this.lblF1.Name = "lblF1";
            this.lblF1.Size = new System.Drawing.Size(21, 15);
            this.lblF1.TabIndex = 0;
            this.lblF1.Text = "F1";
            // 
            // lbxOutput
            // 
            this.lbxOutput.FormattingEnabled = true;
            this.lbxOutput.Location = new System.Drawing.Point(24, 419);
            this.lbxOutput.Name = "lbxOutput";
            this.lbxOutput.Size = new System.Drawing.Size(405, 95);
            this.lbxOutput.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(512, 435);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(90, 27);
            this.button1.TabIndex = 2;
            this.button1.Text = "&Effacer";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingToolStripMenuItem,
            this.miOn});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(654, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // settingToolStripMenuItem
            // 
            this.settingToolStripMenuItem.Name = "settingToolStripMenuItem";
            this.settingToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.settingToolStripMenuItem.Text = "Setting";
            // 
            // miOn
            // 
            this.miOn.BackColor = System.Drawing.Color.Red;
            this.miOn.Name = "miOn";
            this.miOn.Size = new System.Drawing.Size(101, 20);
            this.miOn.Text = "Connection Off";
            this.miOn.Click += new System.EventHandler(this.miOn_Click);
            // 
            // frmControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(654, 517);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lbxOutput);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmControl";
            this.Text = "Remote Control";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblF1;
        private System.Windows.Forms.ListBox lbxOutput;
        private System.Windows.Forms.Label lblP1;
        private System.Windows.Forms.Label lblP5;
        private System.Windows.Forms.Label lblP4;
        private System.Windows.Forms.Label lblDm2;
        private System.Windows.Forms.Label lblP3;
        private System.Windows.Forms.Label lblF8;
        private System.Windows.Forms.Label lblDi2;
        private System.Windows.Forms.Label lblDm1;
        private System.Windows.Forms.Label lblDi1;
        private System.Windows.Forms.Label lblP2;
        private System.Windows.Forms.Label lblF2;
        private System.Windows.Forms.Label lblF3;
        private System.Windows.Forms.Label lblF4;
        private System.Windows.Forms.Label lblF5;
        private System.Windows.Forms.Label lblF6;
        private System.Windows.Forms.Label lblF7;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem settingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem miOn;
    }
}

