﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;

namespace prjWinCsControlRemote
{
    public partial class frmControl : Form
    {
        private SerialPort rs232;
        private delegate void msgDelegate(byte[] b);
        private msgDelegate showReceivedMessage;
        private bool[,] alarmVar;
        private static int ALARMTYPE = 4;
        private static int ALARMNUMERO = 8;
        private bool ConectionStatus = false;

        public frmControl()
        {
            InitializeComponent();
            setStartColor();
            this.alarmVar = new bool[4, 8];
            for (int i = 0; i < ALARMTYPE; i++)
                for (int j = 0; j < ALARMNUMERO; j++)
                    this.alarmVar[i, j] = false;

        }


        private void setStartColor()
        {
            this.lblF1.BackColor = Color.Green;
            this.lblF2.BackColor = Color.Green;
            this.lblF3.BackColor = Color.Green;
            this.lblF4.BackColor = Color.Green;
            this.lblF5.BackColor = Color.Green;
            this.lblF6.BackColor = Color.Green;
            this.lblF7.BackColor = Color.Green;
            this.lblF8.BackColor = Color.Green;
            this.lblP1.BackColor = Color.Green;
            this.lblP2.BackColor = Color.Green;
            this.lblP3.BackColor = Color.Green;
            this.lblP4.BackColor = Color.Green;
            this.lblP5.BackColor = Color.Green;
            this.lblDi1.BackColor = Color.Green;
            this.lblDi2.BackColor = Color.Green;
            this.lblDm1.BackColor = Color.Green;
            this.lblDm2.BackColor = Color.Green;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            lbxOutput.Items.Clear();
        }

        private void Rs232_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            Byte[] msg = new Byte[3];
            for (int i = 0; i < 3; i++)
            {
                msg[i] = Convert.ToByte(this.rs232.ReadByte());
            }

            this.showReceivedMessage = new msgDelegate(ShowMsg);
            this.Invoke(showReceivedMessage, msg);
            this.setAlarmStatus(msg);
        }

        private void setAlarmStatus(byte[] b)
        {
            int type = b[0];
            int number = b[1];
            bool status = (b[2]== 1) ? true:false;

            switch(type)
            {
                case 0:
                    if (number == 0)
                        this.lblF1.BackColor = !status ? Color.Green:Color.Red;
                    if (number == 1)
                        this.lblF2.BackColor = !status ? Color.Green : Color.Red;
                    if (number == 2)
                        this.lblF3.BackColor = !status ? Color.Green : Color.Red;
                    if (number == 3)
                        this.lblF4.BackColor = !status ? Color.Green : Color.Red;
                    if (number == 4)
                        this.lblF5.BackColor = !status ? Color.Green : Color.Red;
                    if (number == 5)
                        this.lblF6.BackColor = !status ? Color.Green : Color.Red;
                    if (number == 6)
                        this.lblF7.BackColor = !status ? Color.Green : Color.Red;
                    if (number == 7)
                        this.lblF8.BackColor = !status ? Color.Green : Color.Red;
                    break;
                case 1:
                    if (number == 0)
                        this.lblP1.BackColor = !status ? Color.Green : Color.Red;
                    if (number == 1)
                        this.lblP2.BackColor = !status ? Color.Green : Color.Red;
                    if (number == 2)
                        this.lblP3.BackColor = !status ? Color.Green : Color.Red;
                    if (number == 3)
                        this.lblP4.BackColor = !status ? Color.Green : Color.Red;
                    if (number == 4)
                        this.lblP5.BackColor = !status ? Color.Green : Color.Red;
                    break;
                case 2:
                    if (number == 0)
                        this.lblDm1.BackColor = !status ? Color.Green : Color.Red;
                    if (number == 1)
                        this.lblDm2.BackColor = !status ? Color.Green : Color.Red;
                    break;
                case 3:
                    if (number == 0)
                        this.lblDi1.BackColor = !status ? Color.Green : Color.Red;
                    if (number == 1)
                        this.lblDi2.BackColor = !status ? Color.Green : Color.Red;
                    break;
            }

        }
        private void ShowMsg(byte[] b)
        {
            String type ="";
            String status = "";
            if (b[0] == 0)
                type = "Fenetre";
            if (b[0] == 1)
                type = "Porte";
            if (b[0] == 2)
                type = "Détecteur de mouvement";
            if (b[0] == 3)
                type = "Détecteur de fumée";
            if (b[2] == 0)
                status = "Activé";
            else
                status = "Désactivée";
            
            lbxOutput.Items.Add(DateTime.Now.ToString("[yyyy-MMM-dd hh:mm:ss] :"));
            lbxOutput.Items.Add("Le "+type+" nombre ("+(b[1]+1)+ ") est "+status);
        }


        private void frmControl_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.rs232.IsOpen)
            {
                this.rs232.Close();
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void miOn_Click(object sender, EventArgs e)
        {
            this.ConectionStatus = !this.ConectionStatus;
            if (this.ConectionStatus)
            {
                miOn.Text = "Connection On";
                miOn.BackColor = Color.Green;
                setOpenPort();
            }
            else
            {
                miOn.Text = "Connection Off";
                miOn.BackColor = Color.Red;
                setClosePort();
            }
        }

        private void setOpenPort()
        {
            string[] listOfPort = SerialPort.GetPortNames();
            Array.Sort(listOfPort);
            this.rs232 = new SerialPort(listOfPort[1], 9600,
                Parity.None, 8, StopBits.One);
            this.rs232.ReceivedBytesThreshold = 3;
            this.rs232.DataReceived += Rs232_DataReceived;

            try
            {
                this.rs232.Open();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);

            }
        }

        private void setClosePort()
        {
            if (this.rs232.IsOpen)
            {
                this.rs232.Close();
                MessageBox.Show("La communication est fermée");
            }
        }
    }
}
